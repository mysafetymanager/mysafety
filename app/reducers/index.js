// @flow
import { combineReducers } from 'redux';
import categoriesReducer from './categories-reducer';

// Root Reducer
const rootReducer = combineReducers({
  categories: categoriesReducer,
});

export default rootReducer;
