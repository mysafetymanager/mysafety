export const categoriesReducer = (state = { categories: [], loading: true }, action) => {
  switch (action.type) {
    case 'GET_CATEGORIS_DATA_LOADING':
      return {
        ...state,                   // keep the existing state,
        loading: true,              // but change loading to true
      };
    case 'GET_CATEGORIES_DATA_RECEIVED':
      return {
        ...state,
        loading: false,             // set loading to false
        categories: action.data.categories, // update categories array with reponse data
      };
    case 'GET_CATEGORIES_DATA_ERROR':
      return state;
    default:
      return state;
  }
};

export default categoriesReducer;
