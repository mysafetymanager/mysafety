import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { StackNavigator } from 'react-navigation';
import login from './components/login/Login';
import Start from './components/forms/Start';
import workouts from './components/forms/Workouts';
import categories from './components/forms/Categories';
import workoutDetails from './components/forms/WorkoutDetails';
import WorkoutRegister from './components/forms/WorkoutRegister';
import TrainerRegister from './components/forms/TrainerRegister';
import TrainerMain from './components/forms/TrainerMain';
import WorkoutInsert from './components/forms/TrainerWorkoutInsert';
import maps from './components/map/MapComponent';
import workoutsMap from './components/map/WorkoutsOnMap';
import { apiMiddleware } from './store/middleware';
import Colors from './components/Colors';
import categoriesReducer from './reducers/categories-reducer';


const headerstyle = { backgroundColor: Colors.HeaderBackground };
const Application = StackNavigator({
  start: {
    screen: Start,
    navigationOptions: {
      title: 'Start',
      headerStyle: headerstyle,
    },
  },
  trainerRegister: {
    screen: TrainerRegister,
    navigationOptions: {
      title: 'Register trainer',
      headerStyle: headerstyle,
    },
  },
  trainerMain: {
    screen: TrainerMain,
    navigationOptions: {
      title: 'trainer main page',
      headerStyle: headerstyle,
    },
  },
  workoutInsert: {
    screen: WorkoutInsert,
    navigationOptions: {
      title: 'workout insert',
      headerStyle: headerstyle,
    },
  },
  categories: {
    screen: categories,
    navigationOptions: {
      title: 'Categories',
      headerStyle: headerstyle,
    },
  },
  workouts: {
    screen: workouts,
    navigationOptions: {
      title: 'Workouts',
      headerStyle: headerstyle,
    },
  },
  workoutsMap: {
    screen: workoutsMap,
    navigationOptions: {
      title: 'Workouts Map',
      headerStyle: headerstyle,
    },
  },
  details: {
    screen: workoutDetails,
    navigationOptions: {
      title: 'Workout Details',
      headerStyle: headerstyle,
    },
  },
  register: { screen: WorkoutRegister },
  maps: { screen: maps },
  login: {
    screen: login,
    navigationOptions: {
      title: 'Login',
      headerStyle: headerstyle,
    },
  },
}, {
  headerMode: 'screen',
  cardStyle: { backgroundColor: Colors.Background },
});

const store = createStore(categoriesReducer, {}, applyMiddleware(apiMiddleware));
store.dispatch({type: 'GET_CATEGORIS_DATA'});
//const store = configureStore({});

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Application />
      </Provider>
    );
  }
}
