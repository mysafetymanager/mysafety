import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, FlatList, Text, Image } from 'react-native';
import MapView, { } from 'react-native-maps';
import Colors from '../Colors';
import BaseListItem from '../visualComponent/BaseListItem';
const moment = require('moment');
const { width, height } = Dimensions.get('window');
const bubbleWidth = width - 40;
const bubbleheight = (height / 4);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  map: {
    flex: 1,
  },
  list:{
    flex: 1,
    position: 'absolute',
    bottom: 10,
    backgroundColor: 'transparent'
  },

  bubble: {
    width: bubbleWidth,
    height: bubbleheight,
    backgroundColor: Colors.CardBackground,
    
    marginLeft: 20,
    marginRight: 20,
    borderColor: Colors.Black,
    borderWidth: 2,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    borderColor: '#D7D7D7',
    borderBottomWidth : 1,
},
avatar: {
    height: 36,
    width: 36,
    borderRadius: 18
},
rowInfo: {
    paddingLeft: 20,
},
row_text: {
  
},
});
class MyListItem extends BaseListItem {
  randerItem() {
    return (
      <View style={styles.bubble}>
       <View style={styles.row}>
            <Image
                source= {require('../../images/ocot.png')}
                style={styles.avatar}/>
             <View style={styles.rowInfo}>
                <Text style={styles.row_text}>{this.props.data.category}</Text>
                <Text style={styles.row_text}>{this.props.data.desc}</Text>
                <Text style={styles.row_text}>{this.props.data.date}</Text>
                <Text style={styles.row_text}>{moment(this.props.data.date).fromNow()}</Text>
            </View>
        </View>
      </View>
    );
  }
}
export default class WorkoutOnMap extends Component {
    constructor(props){
        super(props);
        this.state = {
            workouts1 : [{_id:'a'}, {_id:'b'},{_id:'c'}],
            workouts: props.workouts,
        }
    } componentDidMount(){
        this._loadInitialState().done();
    }

    _loadInitialState = async () => {
        this.getWorkouts();
    }

    _keyExtractor = (item, index) => item._id;
    
    _renderItem = ({item}) => (
        <MyListItem
            id={item._id}
            onPressItem = {(item)=>this._onPressItem(item)}
            data = {item}
           
        />
    );

  render() {
    return (
      <View style={styles.container} >
        <MapView style={styles.map} />
        <FlatList
          style={styles.list}
         horizontal={true}
         pagingEnabled={true}
          data={this.state.workouts}
          extraData={this.state}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
        />
      </View>
    );
  }


  getWorkouts = () => {
    fetch('http://18.218.220.192:3000/workouts/search1',{
        method:'get',
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then((response) => {
            const json = response.json();
            console.log('workouts: ' +response);
            return json;
            })
        .then((res) => {
                this.setState(
                    {
                        workouts: res.workouts,
                        dataSource: res.workouts
                    });
        })
        .done()
}
}
