import React, { Component } from 'react';
import {
  Platform,
  AppRegistry,
  PermissionsAndroid,
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Text,
} from 'react-native';

import MapView, {
  MAP_TYPES,
  ProviderPropType,
  PROVIDER_GOOGLE,
  Marker,
  AnimatedRegion,
} from 'react-native-maps';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

function randomColor() {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  map: {
    flex: 1
  },
  bubble: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default class MapComponent extends Component {
  constructor(props) {
    super(props);
    // this.requestCameraPermission.bind(this);
    this.state = {
      provider: PROVIDER_GOOGLE,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      coordinate: new AnimatedRegion({
        latitude: LATITUDE,
        longitude: LONGITUDE,
      }),
      markers: [],
    };
  }

  componentDidMount() {
    // this.requestCameraPermission();
  }

  //   async requestCameraPermission() {
  //     console.log('tring request permmision');
  //     try {
  //       const granted = await PermissionsAndroid.request(
  //         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //         {
  //           title: 'Cool Photo App Camera Permission',
  //           message:
  //             'Cool Photo App needs access to your camera so you can take awesome pictures.'
  //         }
  //       );
  //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //         console.log('You can use the camera');
  //       } else {
  //         console.log('Camera permission denied');
  //       }
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   }

  //   gotoCurrentLocation() {
  //     console.log('gotocurrent: ${  this.state.myLocation.longitude  }, ${  this.state.myLocation.latitude}');
  //     this.map.animateToRegion({
  //       latitude: this.state.myLocation.latitude,
  //       longitude: this.state.myLocation.longitude,
  //       latitudeDelta: 0.0922,
  //       longitudeDelta: 0.0421,
  //     });
  //   }

  //   onMapReady() {
  //     this.navigator.geolocation.getCurrentPosition((location) => {
  //       console.log('locaion: ', location);
  //       this.setState({
  //         myLocation: location.coords,
  //       });
  //       const region = {
  //         latitude: location.coords.latitude,
  //         longitude: location.coords.longitude,
  //         latitudeDelta: 0.5,
  //         longitudeDelta: 0.5,
  //       };
  //       // this.onRegionChange(region, region.latitude, region.longitude);
  //       this.map.animateToRegion(region, 1);
  //       // this.gotoCurrentLocation();
  //     });
  //   }
  onMapPress(e) {
    this.setState({
      markers: [
        ...this.state.markers,
        {
          coordinate: e.nativeEvent.coordinate,
          key: id++,
          color: randomColor(),
        },
      ],
    });
  }
  animate() {
    const { coordinate } = this.state;
    const newCoordinate = {
      latitude: LATITUDE + ((Math.random() - 0.5) * (LATITUDE_DELTA / 2)),
      longitude: LONGITUDE + ((Math.random() - 0.5) * (LONGITUDE_DELTA / 2)),
    };

    if (Platform.OS === 'android') {
      if (this.state.markers) {
          this.state.markers[0]._component.animateMarkerToCoordinate(newCoordinate, 500);
      }
    } else {
      coordinate.timing(newCoordinate).start();
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={this.state.provider}
          ref={(ref) => {
            this.map = ref;
          }}
          style={styles.map}
          initialRegion={this.state.region}
          //onRegionChange={region => this.onRegionChange(region)}
          onPress={e => this.onMapPress(e)}
        >
          {this.state.markers.map(marker => (
            <Marker
              key={marker.key}
              coordinate={marker.coordinate}
              pinColor={marker.color}
              onPress={(marker) => this.onMarkerPress(marker)}
            />
          ))}
        </MapView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => this.animate()}
            style={[styles.bubble, styles.button]}
          >
            <Text>Animate</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

MapComponent.propTypes = {
  provider: ProviderPropType
};

AppRegistry.registerComponent('MapComponent', () => MapComponent);
