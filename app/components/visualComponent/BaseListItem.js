import React, { Component } from 'react';
import { StyleSheet,View, TouchableHighlight, Image, Text, AsyncStorage,TouchableOpacity, FlatList} from 'react-native';
import Colors from '../Colors'

export default class BaseListItem extends React.PureComponent{
    _onPress = () => {
        this.props.onPressItem(this.props.data);
    };
    render() {
        return (
            <TouchableHighlight
                onPress={this._onPress}
                underlayColor={Colors.PrimaryLigth}
            >
                {this.randerItem()}
            </TouchableHighlight>);
    }
    randerItem(){
        return(<View/>);
    }
}