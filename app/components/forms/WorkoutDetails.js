import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, AsyncStorage } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#FFF',
    fontWeight: 'bold',
  },
});

export default class WorkoutDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workout: '',
      login: false,
    };
  }

  componentDidMount() {
    this.loadInitialState().done();
  }

  async loadInitialState() {
    const value = await AsyncStorage.getItem('user');
    if (value !== null) {
      this.setState({ login: true });
    }
  }

  registerWorkout() {
    if (this.state.login) {
      this.props.navigation.navigate('register', {
        workout: this.state.workout,
      });
    } else {
      this.props.navigation.navigate('login', {
        workout: this.state.workout,
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>{this.state.workout.desc}</Text>
        <Button
          onPress={() => this.registerWorkout()}
          title="register workout"
          color="#841584"
          accessibilityLabel="register this wotkout"
        />
      </View>
    );
  }
}
