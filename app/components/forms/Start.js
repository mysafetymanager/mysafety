import React, { Component } from 'react';
import { StyleSheet, Button, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#FFF',
    fontWeight: 'bold'
  },
});

export default class Start extends Component {
  directSweatersPage() {
    this.props.navigation.navigate('categories');
  }

  directTrainersPage() {
    this.props.navigation.navigate('trainerRegister');
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="sweaters" onPress={() => this.directSweatersPage()} />
        <Button title="trainers" onPress={() => this.directTrainersPage()} />
      </View>
    );
  }
}
