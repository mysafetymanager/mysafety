import React, { Component } from 'react';
import { StyleSheet, Button, View, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#FFF',
    fontWeight: 'bold'
  },
});

export default class TrainerMain extends Component {

  directToWorkoutInsert(){
    this.props.navigation.navigate('workoutInsert');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>list of trainer workouts</Text>
        <Button title="workout insert" onPress={() => this.directToWorkoutInsert()} />
      </View>
    );
  }
}
