import React, { Component } from 'react';
import { StyleSheet, Button, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#FFF',
    fontWeight: 'bold'
  },
});

export default class TrainerRegister extends Component {
  directToTrainerMainPage() {
    this.props.navigation.navigate('trainerMain');
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="register trainer" onPress={() => this.directToTrainerMainPage()} />
      </View>
    );
  }
}
