import React, { Component } from 'react';
import { StyleSheet, Button, Text, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#FFF',
    fontWeight: 'bold'
  },
});

export default class Start extends Component {
  workoutInsert() {
    alert('workout insert');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Workout insert</Text>
        <Button title="workout insert" onPress={() => this.workoutInsert()} />
      </View>
    );
  }
}
