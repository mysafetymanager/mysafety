/**
 * Created by zeev on 11/06/18.
 */
import React, { Component } from 'react';
import { StyleSheet,View, TouchableHighlight, Image, Text, Button, AsyncStorage,TouchableOpacity, FlatList} from 'react-native';
import BaseListItem from '../visualComponent/BaseListItem';
import Colors from '../Colors'
const moment = require('moment');

class MyListItem extends BaseListItem {
    randerItem() {
        return (
        <View style={listItemStyles.row}>
            <Image
                source= {require('../../images/ocot.png')}
                style={listItemStyles.avatar}/>
             <View style={listItemStyles.rowInfo}>
                <Text style={listItemStyles.row_text}>{this.props.data.category}</Text>
                <Text style={listItemStyles.row_text}>{this.props.data.desc}</Text>
                <Text style={listItemStyles.row_text}>{this.props.data.date}</Text>
                <Text style={listItemStyles.row_text}>{moment(this.props.data.date).fromNow()}</Text>
            </View>
        </View>
        );
    }
}

const listItemStyles = StyleSheet.create({
    row: {
        flex: 1,
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
        borderColor: '#D7D7D7',
        borderBottomWidth : 1,
    },
    avatar: {
        height: 36,
        width: 36,
        borderRadius: 18
    },
    rowInfo: {
        paddingLeft: 20,
    },
    row_text: {
      
    },
});

export default class Workouts extends Component {
    constructor(props){
        super(props);
        this.state = {
            filter1 : '',
            filter2 : '',
            dataSource : '',
            selected: (new Map(): Map<string, boolean>),
            workouts: ''
        }
    }

    static navigationOptions = ({navigation}) => {
        return{
            headerRight:(
                <TouchableHighlight
                    style={{ padding: 10}}
                    onPress={()=> navigation.navigate('workoutsMap')}>     
                    <Image 
                    style={{ width: 30, height: 30}}
                    source= {require('../../images/ocot.png')} />
                 </TouchableHighlight> 
            )
        }
    };

    componentDidMount(){
        this._loadInitialState().done();
    }

    _keyExtractor = (item, index) => item._id;

    _loadInitialState = async () => {
        this.getWorkouts();
    }


    _onPressItem = (item) => {

        // updater functions are preferred for transactional updates
        this.setState((state) => {
            this.props.navigation.navigate('details',{
                item: item   //your user details
            });
        });
    };

    _renderItem = ({item}) => (
        <MyListItem
            id={item._id}
            onPressItem = {(item)=>this._onPressItem(item)}
            data = {item}
            selected={!!this.state.selected.get(item._id)}
            title={item.category}
        />
    );

    render() {
        return (
            <View style={styles.container} >
                <FlatList
                    data={this.state.dataSource}
                    extraData={this.state}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        );
    }

    getWorkouts = () => {
        fetch('http://18.218.220.192:3000/workouts/search1',{
            method:'get',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                const json = response.json();
                console.log('workouts: ' +response);
                return json;
                })
            .then((res) => {
                    this.setState(
                        {
                            workouts: res.workouts,
                            dataSource: res.workouts
                        });
            })
            .done()
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'flex-start',
        paddingLeft: 10,
        paddingRight: 10,
    },
})