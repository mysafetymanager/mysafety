import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2896d3',
    paddingLeft: 40,
    paddingRight: 40,
  },
  header: {
    fontSize: 24,
    marginBottom: 60,
    color: '#FFF',
    fontWeight: 'bold'
  },
});

export default class WorkoutRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      workout: props.navigation.state.params.workout,
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.header}>REGISTER {this.state.workout.desc}</Text>
      </View>
    );
  }
}
