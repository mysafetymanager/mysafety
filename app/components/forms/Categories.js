import React, { Component } from 'react';
import { StyleSheet,View, Dimensions, Image, Text, AsyncStorage,TouchableOpacity, FlatList} from 'react-native';
import Colors from '../Colors'
import BaseListItem from '../visualComponent/BaseListItem'
import { connect } from 'react-redux';

const {height, width} = Dimensions.get('window');
const itemWidth = (width - 50) / 2;

class MyListItem extends BaseListItem {
  randerItem() {
    return (
      <View style={listItemStyles.box}>
        <Image
          source= {require('../../images/ocot.png')}
          style={listItemStyles.avatar}/>
        <View style={listItemStyles.rowInfo}>
          <Text style={listItemStyles.row_text}>{this.props.data.title}</Text>
        </View>
      </View>);
    }
}

const listItemStyles = StyleSheet.create({
    box: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      minWidth: itemWidth,
      maxWidth: itemWidth,
      height: itemWidth,
      margin: 5,
      backgroundColor: Colors.Primary
    },
    avatar: {
        height: 36,
        width: 36,
        borderRadius: 18,
        
    },
    rowInfo: {
    },
    row_text: {
    },
});


class Categories extends Component {
    constructor(props){
        super(props);
        const { categories } = this.props;
        this.state = {
            categories : '',
            selected: (new Map(): Map<string, boolean>)
        }
    }

    componentDidMount(){
    }

    _keyExtractor = (item, index) => item._id;


    _onPressItem = (item) => {
        this.setState((state) => {
            this.props.navigation.navigate('workouts',{
                item: item
            });
        });
    };

    _renderItem = ({item}) => (
        <MyListItem
            id={item._id}
            onPressItem = {(item)=>this._onPressItem(item)}
            data = {item}
            selected={!!this.state.selected.get(item._id)}
            title={item.category}
        />
    );

    render() {
        return (
            <View style={styles.container} >
                <FlatList
                    contentContainerStyle={styles.list}
                    data={this.props.categories}
                    extraData={this.state}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'flex-start',
        paddingLeft: 10,
        paddingRight: 10,
    },
    list: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',

    }
});


export default connect(
    state => (
        {
        categories: state.categories,
    }),
    // dispatch => ({
    //     refresh: () => dispatch({type: 'GET_CATEGORY_DATA'}),
    // }),
)(Categories)