module.exports = Object.freeze({
  Background: '#f1f1f1',
  Primary: '#6200EE',
  PrimaryDark: '#3700B3',
  PrimaryLigth: '#BB86FC',
  Secondary: '#03DAC6',
  SecondaryDark: '#018786',
  Error: '#B00020',
  White: '#FFFFFF',
  Black: '#000000',
  HeaderBackground: '#BB86FC',
  CardBackground: '#6200EE5F',
});

