// @flow
import { fetchCategoriesData } from '../../services/http-requests';
import { fetchDataError } from './fetch-data-error';
import { fetchDataRequest } from './fetch-data-request';
import { fetchDataSuccess } from './fetch-data-success';

export const fetchData = () => (
    (dispatch: Function) => {
        dispatch(fetchDataRequest());
        return fetchCategoriesData()
            .then((categoriesInfo) => dispatch(fetchDataSuccess(categoriesInfo)))
            .catch(() => dispatch(fetchDataError()));
    }
);