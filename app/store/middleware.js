export const apiMiddleware = store => next => action => {
// Pass all actions through by default
  next(action);
  switch (action.type) {
    // In case we receive an action to send an API request
    case 'GET_CATEGORIS_DATA':
      store.dispatch({type: 'GET_CATEGORIS_DATA_LOADING' });
      fetch('http://18.218.220.192:3000/categories', { method: 'get', headers: { 'Content-Type': 'application/json' } })
        .then(response => response.json())
        .then(data => next({ type: 'GET_CATEGORIES_DATA_RECEIVED', data }))
        .catch(error => next({ type: 'GET_CATEGORIES_DATA_ERROR', error }));
      break;
    default:
      break;
  }
};
